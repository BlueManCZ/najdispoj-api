# Najdispoj API

Backend portion of najdispoj.sk journey planner, that's based on FastAPI. It's basically a wrapper around OpenTripPlanner and Photon, that should be easily extendable in the future. Currently written to suite Debian-based distributions. You will need to write your own GTFS-fetching routine, as the current one is vendor-specific.

This project uses the following APIs by default:
- [Overpass API](http://overpass-api.de/)
- [Photon](https://photon.komoot.io/)

Please make sure that your usage complies with their corresponding terms of usage. In case of heavy usage, consider running your own instance or switching to a paid for service.

For pfaedle's building instructions see [pfaedle's GitHub repository](https://github.com/ad-freiburg/pfaedle)
OpenTripPlanner's official documentation is available [here](http://docs.opentripplanner.org/en/v1.5.0/). Please note that najdispoj.sk is currently using OTP version 1.5.0. This should change in the near future with an upgrade to 2.0.0.

After you're done downloading and/or building these two tools, take a look at api.py, core.py and config.py - the code is quite simple and self-explanatory. You will probably not avoid tampering with core.py and api.py, so don't worry if the options available in config.py are not good enough for you. There's also some tool-specific config files laying around in the subfolders, which you probably won't need to touch just to get going.

### These are some other tools you will need to have installed to proceed:

    pip3 install paramiko
    pip3 install requests
    pip3 install fastapi
    pip3 install uvicorn[standard]
    sudo apt-get install default-jre
    sudo apt-get install osmium-tool
    sudo apt-get install osmctools
    sudo apt-get install ufw

In case anything else is missing, just act according to the error messages and you should be fine.
More specific documentation is coming soon.

Enjoy!

Released under Apache License 2.0
