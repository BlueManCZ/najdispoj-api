import urllib.request
import os
import paramiko
import time
import shutil
import config as cfg
import sys


def download_osm(path, path_raw):
    # Download raw OSM data (.osm file)

    print('Downloading raw OpenStreetMap data from overpass-api.de...')
    start = time.time()

    os.makedirs(path, exist_ok=True)

    url = 'https://overpass-api.de/api/map?bbox=' \
        + str(cfg.bbox[0]['lng']) + ',' + str(cfg.bbox[0]['lat']) + ',' \
        + str(cfg.bbox[1]['lng']) + ',' + str(cfg.bbox[1]['lat'])
    urllib.request.urlretrieve(url, path_raw)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def alter_osm(path_from, path_to):
    # Create OSM file for OTP routing
    # By replacing highway=residential by highway=cycleway we
    # make sidewalks the (slightly more) preferred option for walking.
    # In the future, create a pull request to OTP repository and remove this
    # workaround

    print('Altering data...')
    start = time.time()

    os.system(
        'osmfilter ' +
        path_from +
        ' --modify-tags="highway=residential to =cycleway" -o=' +
        path_to)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def crop_osm(path_from, path_to):
    # Crop OSM data

    print('Cropping data...')
    start = time.time()

    os.system('osmium extract --strategy complete_ways --bbox ' +
              str(cfg.bbox[0]['lng']) +
              ',' +
              str(cfg.bbox[0]['lat']) +
              ',' +
              str(cfg.bbox[1]['lng']) +
              ',' +
              str(cfg.bbox[1]['lat']) +
              ' ' +
              path_from +
              ' -o ' +
              path_to +
              ' --overwrite')

    end = time.time()
    print('Done. Elapsed time:', end - start)


def filter_osm(path_from, path_to):
    # Filter OSM data

    print('Filtering data...')
    start = time.time()

    os.system(
        'osmium tags-filter ' +
        path_from +
        ' w/highway=path,footway,steps,residential,service,unclassified,living_street,pedestrian,track,corridor,bridleway,cycleway w/footway w/sidewalk w/public_transport=platform w/railway=platform ' +
        ' w/park_ride=yes r/type=restriction -o ' +
        path_to +
        ' -f pbf,add_metadata=false --overwrite')

    end = time.time()
    print('Done. Elapsed time:', end - start)


def download_gtfs(path):
    # Download GTFS data

    # Helper function for parsing folder names into dates
    def get_date(folder):
        folder = folder.split('_')
        return folder[1]

    print('Downloading GTFS data...')
    start = time.time()

    # Create destination directory
    os.makedirs(path, exist_ok=True)

    ssh = paramiko.SSHClient()

    # Add key
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh.connect(cfg.gtfs_host, username=cfg.gtfs_username,
                password=cfg.gtfs_password)
    ftp = ssh.open_sftp()

    files = ftp.listdir()
    gtfs_folders = list(filter(lambda f: f.startswith('GTFS_'), files))
    gtfs_dates = list(map(get_date, gtfs_folders))
    gtfs_current = max(gtfs_dates)
    gtfs_folder = 'GTFS_' + str(gtfs_current) + '/'

    gtfs_files = ftp.listdir(gtfs_folder)
    for file in gtfs_files:
        ftp.get(gtfs_folder + file, path + file)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def alter_gtfs(path):
    # Alter GTFS data

    def repair_feed_info():
        tmp = open(path + 'feed_info.txt.tmp', 'w')

        with open(path + 'feed_info.txt', 'r') as file:
            i = 1

            for line in file:
                if i == 2:
                    values = line.split(',')
                    values[2] = '"https://dpb.sk/"'
                    line = ",".join(values)

                tmp.write(line)
                i += 1

        os.remove(path + 'feed_info.txt')
        os.rename(path + 'feed_info.txt.tmp', path + 'feed_info.txt')

    def repair_routes():
        # 800 is the trolleybus route_type in Extended GTFS Route Types:
        # https://developers.google.com/transit/gtfs/reference/extended-route-types
        # 11 is the route_type according to these docs:
        # https://developers.google.com/transit/gtfs/reference#routestxt
        # OTP refuses to build graph unless this gets replaced
        # TODO look deeper into this issue

        tmp = open(path + 'routes.txt.tmp', 'w')

        with open(path + 'routes.txt', 'r') as file:
            i = 1

            for line in file:
                if i != 1:
                    values = line.split(',')

                    if values[5] == "11":
                        values[5] = "800"

                    line = ",".join(values)

                tmp.write(line)
                i += 1

        os.remove(path + 'routes.txt')
        os.rename(path + 'routes.txt.tmp', path + 'routes.txt')

    print('Altering GTFS data...')
    start = time.time()

    repair_feed_info()
    repair_routes()

    end = time.time()
    print('Done. Elapsed time:', end - start)


def enhance_gtfs(path):
    print('Enhancing GTFS data...')
    start = time.time()

    os.system(cfg.pfaedle_run_cmd + path)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def zip_gtfs(path_from, path_to):
    # Zip GTFS data

    print('Zipping GTFS data...')
    start = time.time()

    shutil.make_archive(path_to, 'zip', path_from)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def build_graph(path):
    # Build OTP graph

    print('Building graph...')
    start = time.time()

    os.makedirs(path, exist_ok=True)
    os.system(cfg.otp_build_cmd)

    end = time.time()
    print('Done. Elapsed time:', end - start)


def run_otp():
    # Run OTP to serve graph

    print('Running OTP in the background. Use "screen -r otp" to switch to its screen.')
    cmd = 'screen -dmS otp ' + cfg.otp_run_cmd
    os.system(cmd)


def run_api():
    # Run API to handle requests

    print('Running API in the background. Use "screen -r api" to switch to its screen.')
    cmd = 'screen -dmS api ' + cfg.api_run_cmd
    os.system(cmd)

def kill_otp():
    # Kill OTP process (and screen)

    print('Killing OTP...')
    cmd = 'screen -S otp -X quit'
    os.system(cmd)

def kill_api():
    # Kill API process (and screen)

    print('Killing API...')
    cmd = 'screen -S api -X quit'
    os.system(cmd)

# Command line arguments

if '-updateOSM' in sys.argv:
    download_osm(cfg.osm_path, cfg.osm_raw_path)
    alter_osm(cfg.osm_raw_path, cfg.osm_altered_path)
    filter_osm(cfg.osm_altered_path, cfg.osm_filtered_path)

if '-updateGTFS' in sys.argv:
    download_gtfs(cfg.gtfs_raw_folder)
    alter_gtfs(cfg.gtfs_raw_folder)
    enhance_gtfs(cfg.gtfs_raw_folder)
    zip_gtfs(cfg.gtfs_raw_folder, cfg.gtfs_latest_zip)

if '-buildGraph' in sys.argv:
    build_graph(cfg.otp_folder)

if '-kill' in sys.argv:
    kill_otp()
    kill_api()

if '-serve' in sys.argv:
    kill_otp()
    kill_api()
    run_otp()
    run_api()

if '-restartAPI' in sys.argv:
    kill_api()
    run_api()
