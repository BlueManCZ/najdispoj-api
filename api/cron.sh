#!/bin/bash

# Set up using e.g. Crontab
#
# apt-get install cron
# crontab -e
#
# Add this line and save the file:
# 0 3 * * * /path/to/api/cron.sh

cd /home/najdispoj/najdispoj/api
python3 core.py -serve