import requests
import json
import math
import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

import config as cfg

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*']
)


def create_plan(data):
    if 'plan' not in data:
        return {
            'itineraries': [],
            'empty': True
        }
    plan = data['plan']

    if 'itineraries' not in plan:
        return {
            'itineraries': [],
            'empty': True
        }
    itineraries = plan['itineraries']

    plan = {
        'itineraries': [],
        'empty': len(itineraries) == 0
    }

    for itinerary in itineraries:
        plan['itineraries'].append(create_itinerary(itinerary))

    if len(plan['itineraries']) > 1:
        plan['itineraries'] = plan['itineraries'][1:]

    return plan


def create_itinerary(itinerary):
    it = {
        'legs': [],
        'duration': itinerary['duration'],
        'departure': itinerary['startTime'],
        'arrival': itinerary['endTime'],
    }

    legs = itinerary['legs']
    for index, leg in enumerate(legs):
        last = len(legs) - 1 == index
        it['legs'].append(create_leg(leg, last))

    return it


def create_leg(leg, last):
    def leg_mode(t, number):
        if t == 'WALK':
            return 'walk'
        if t == 'TRAM':
            return 'tram'
        if t == 'BUS':
            if len(number) == 3 and number[0] != 'N':
                return 'trolley'
            else:
                return 'bus'

    if last:
        duration = leg['duration'] - 60
    else:
        duration = leg['duration']

    l = {
        'mode': '',
        'duration': duration,
        'distance': leg['distance'],
        'path': leg['legGeometry']['points'],
        'departure': leg['startTime'],
        'arrival': leg['to']['arrival'],
        'number': '',
        'stops': []
    }

    if 'routeShortName' in leg:
        l['number'] = leg['routeShortName']

    l['mode'] = leg_mode(leg['mode'], l['number'])

    l['stops'].append(create_stop(leg['from']))
    if 'intermediateStops' in leg:
        for stop in leg['intermediateStops']:
            l['stops'].append(create_stop(stop))
    l['stops'].append(create_stop(leg['to'], True))

    return l


def create_coords(lat, lng):
    return {
        'lat': lat,
        'lng': lng
    }


def create_stop(stop, last=False):
    def stop_name(n):
        if n == 'Origin' or n == 'Destination':
            return ''
        return n

    return {
        'name': stop_name(stop['name']),
        'coords': create_coords(stop['lat'], stop['lon']),
        'departure': stop['arrival'] if last else stop['departure']
    }


def add_timeline(itinerary, max_duration):
    for leg in itinerary['legs']:
        leg['timelineDuration'] = math.floor(
            leg['duration'] / max_duration * 100)


def add_timelines(plan):
    max_duration = 0
    for itinerary in plan['itineraries']:
        if itinerary['duration'] > max_duration:
            max_duration = itinerary['duration']

    for itinerary in plan['itineraries']:
        itinerary['timelineDuration'] = math.floor(
            itinerary['duration'] / max_duration * 100)
        add_timeline(itinerary, max_duration)


def add_waiting(itinerary):
    legLen = len(itinerary['legs'])

    for index, leg in enumerate(itinerary['legs']):
        if index < legLen - 2 and index != 0:
            next_leg = itinerary['legs'][index + 1]
            if leg['mode'] == 'walk':
                waitingDuration = math.floor(
                    (int(next_leg['departure']) - int(leg['arrival'])) / 1000)
            else:
                waitingDuration = math.floor(
                    (int(next_leg['departure']) - int(leg['stops'][len(leg['stops']) - 1]['departure'])) / 1000)

            if waitingDuration > 60 and leg['mode'] == 'walk' and next_leg['mode'] != 'walk':
                waitingDuration -= 60

            leg['waitingDuration'] = waitingDuration


def add_waitings(plan):
    for itinerary in plan['itineraries']:
        add_waiting(itinerary)


@app.get('/plan')
def get_plan(
        origin: str,
        destination: str,
        arriveBy: bool,
        date: str,
        time: str,
        maxTransfers: int,
        maxWalkDistance: int,
        walkSpeed: int,
        optimize: str):
    origin = json.loads(origin)
    destination = json.loads(destination)
    walkSpeedOptions = [0.5, 1.11, 2.22, 4.44]  # meters/second
    optimizeOptions = {'quick': 'QUICK', 'transfers': 'TRANSFERS'}

    params = {
        'fromPlace': str(origin['lat']) + ',' + str(origin['lng']),
        'toPlace': str(destination['lat']) + ',' + str(destination['lng']),
        'time': time,
        'date': date,
        'showIntermediateStops': True,
        'arriveBy': arriveBy,
        'numItineraries': 11,  # first one is always a walking itinerary, which gets removed later
        'minTransferTime': 120,
        'maxTransfers': maxTransfers,
        'maxWalkDistance': maxWalkDistance,
        'walkSpeed': walkSpeedOptions[walkSpeed],
        'optimize': optimizeOptions[optimize],
        'mode': "TRANSIT,WALK",
        'transferSlack': 0,
        'boardSlack': 0,
        'alightSlack': 0,
    }
    # TODO include these parameters in query:
    # wheelchair, locale

    # Get data from OTP
    data = requests.get(
        'http://0.0.0.0:25515/otp/routers/default/plan?', params=params)
    data = json.loads(data.content)

    plan = create_plan(data)
    add_timelines(plan)
    add_waitings(plan)

    return {'plan': plan}


def geojson_name(obj):
    # Takes dict as input

    name = {
        'primary': '',
        'secondary': ''
    }

    props = obj['properties']

    if 'name' in props:
        name['primary'] = props['name']
    elif 'street' in props and 'housenumber' in props:
        name['primary'] = props['street'] + ' ' + props['housenumber']
    else:
        name['primary'] = 'Miesto bez názvu'

    if 'locality' in props:
        name['secondary'] = props['locality']

    return name

# Takes dict as input


def geojson_type(obj):
    props = obj['properties']

    if 'osm_key' in props:
        print(props['osm_key'])
        if props['osm_key'] == 'highway':
            return 'street'

        if props['osm_key'] == 'shop':
            return 'shop'

        if props['osm_key'] == 'amenity':
            return 'institution'

    if 'osm_value' in props:
        if props['osm_value'] == 'bus_stop' or props['osm_value'] == 'platform':
            return 'stop'

        if props['osm_value'] in [
            'town',
            'neighbourhood',
            'suburb',
                'village']:
            return 'town'

        if props['osm_value'] in ['residential']:
            return 'building'

        if props['osm_value'] in ['school']:
            return 'institution'

    if 'street' in props and 'housenumber' in props:
        return 'building'

    return 'other'


def sort_places(results):
    def sortFunc(place):
        typeRatings = {
            'stop': 0,
            'institution': 1,
            'shop': 2,
            'building': 3,
            'street': 4,
            'other': 5
        }
        if place['type'] in typeRatings:
            return typeRatings[place['type']]
        else:
            return 100

    results.sort(key=sortFunc)


@app.get('/autocomplete')
def get_autocomplete(query: str):
    params = {
        'q': query,
        'limit': 10,
        'bbox': str(
            cfg.bbox[0]['lng']) + ',' + str(
            cfg.bbox[0]['lat']) + ',' + str(
                cfg.bbox[1]['lng']) + ',' + str(
                    cfg.bbox[1]['lat']),
        'lat': cfg.center_lat,
        'lon': cfg.center_lng,
        'location_bias_scale': 4,
        'osm_tag': [
            '!boundary',
            ':!unclassified',
            ':!town',
            ':!neighbourhood']}

    # Get data from Komoot Photon API
    photonData = requests.get(
        'https://photon.komoot.io/api',
        params=params).json()

    # Modify photonData to our format
    used_ids = []
    results = []
    features = photonData['features']
    for place in features:
        coords = {'lat': place['geometry']['coordinates'][1],
                  'lng': place['geometry']['coordinates'][0]}
        place_id = place['properties']['osm_id']

        if place_id not in used_ids:
            used_ids.append(place_id)
            results.append({'coords': coords,
                            'name': geojson_name(place),
                            'type': geojson_type(place)})

    sort_places(results)

    return {'query': query, 'places': results}


@app.get('/reverse')
async def get_reverse(coords: str):
    coords = json.loads(coords)
    params = {
        'lat': str(coords['lat']),
        'lon': str(coords['lng'])
    }

    data = requests.get(
        'https://photon.komoot.io/reverse',
        params=params).json()

    return {'name': geojson_name(data['features'][0])}

if __name__ == "__main__":
    os.system('uvicorn api:app --reload --host 0.0.0.0 --port ' +
              str(cfg.api_port) +
              ' --ssl-keyfile ' +
              cfg.api_key_path +
              ' --ssl-certfile ' +
              cfg.api_cert_path)
